﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace AINU.UserControls
{
    /// <summary>
    /// Interaction logic for Insurance.xaml
    /// </summary>
    public partial class Insurance : UserControl
    {
        public Insurance()
        {
            InitializeComponent();
        }
        public event EventHandler EvntCloseInsurance;
        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntCloseInsurance(this, null);
        }

        private void svInsurance_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
