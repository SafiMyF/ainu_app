﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AINU.UserControls
{
    /// <summary>
    /// Interaction logic for HealthTipItemView.xaml
    /// </summary>
    public partial class HealthTipItemView : UserControl
    {
        public HealthTipItemView()
        {
            InitializeComponent();
        }
        public Button PlanViewImg;
        public Image imgTemp;
        public Image imgTemp1;
        public event EventHandler EvntCloseHealthTipIV;

        private void btnClose_TouchDown(object sender, TouchEventArgs e)
        {
            //event to close HalthTipItemView.xaml
            EvntCloseHealthTipIV(this, null);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (PlanViewImg != null)
            {
                grdImg.Children.Clear();    // clear grdImg Children
                imgTemp = new Image();
                imgTemp1 = PlanViewImg.Content as Image;    // pass button content as image to imgTemp1
                imgTemp.Source = imgTemp1.Source;
                grdImg.Children.Add(imgTemp);   //add image to grdImg
            }
        }
    }
}
