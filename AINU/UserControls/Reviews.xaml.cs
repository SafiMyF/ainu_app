﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AINU.UserControls
{
    /// <summary>
    /// Interaction logic for Reviews.xaml
    /// </summary>
    public partial class Reviews : UserControl
    {
        public Reviews()
        {
            InitializeComponent();
        }
        public event EventHandler EvntCloseReviews;
        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntCloseReviews(this, null);
        }
        private void btnPatientTestimonial_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            //Storyboard to open Patient Review
            Storyboard OpenPatientTestimonial_SB = TryFindResource("OpenPatientTestimonial_SB") as Storyboard;
            OpenPatientTestimonial_SB.Begin();
        }
        private void btnGuestBook_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            //Storyboard to open Guest Review
            Storyboard OpenGuestBook_SB = TryFindResource("OpenGuestBook_SB") as Storyboard;
            OpenGuestBook_SB.Begin();
        }

        private void btnBackFromPtTestimonial_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            //Storyboard to close Patient Review
            Storyboard BackFrmPatientTestimonial_SB = TryFindResource("BackFrmPatientTestimonial_SB") as Storyboard;
            BackFrmPatientTestimonial_SB.Begin();
        }

        private void btnBackFromGuestBook_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            //Storyboard to close Guest Review
            Storyboard BackFrmGuestBook_SB = TryFindResource("BackFrmGuestBook_SB") as Storyboard;
            BackFrmGuestBook_SB.Begin();            
        }

        private void btnWritePatientTestimonial_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            //Storyboard to open Patient Review Popup
            Storyboard OpenWritePatientReview_SB = TryFindResource("OpenWritePatientReview_SB") as Storyboard;
            OpenWritePatientReview_SB.Begin();            
        }

        private void btnWriteGuestTestimonial_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            //Storyboard to open Guest Review Popup
            Storyboard OpenWriteGuestReview_SB = TryFindResource("OpenWriteGuestReview_SB") as Storyboard;
            OpenWriteGuestReview_SB.Begin();
        }

        private void btnCloseWritePatientReview_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            //Storyboard to close Patient Review Popup
            Storyboard CloseWritePatientReview_SB = TryFindResource("CloseWritePatientReview_SB") as Storyboard;
            CloseWritePatientReview_SB.Begin();  
        }

        private void btnCloseWriteGuestReview_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            //Storyboard to close Guest Review Popup
            Storyboard CloseWriteGuestReview_SB = TryFindResource("CloseWriteGuestReview_SB") as Storyboard;
            CloseWriteGuestReview_SB.Begin();
        }

        private void svGuestBook_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void btnSavePatientComment_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            //Storyboard to close Patient Review Popup after saving
            Storyboard CloseWritePatientReview_SB = TryFindResource("CloseWritePatientReview_SB") as Storyboard;
            CloseWritePatientReview_SB.Begin();
        }

        private void btnSaveGuestComment_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            //Storyboard to close Guest Review Popup after saving
            Storyboard CloseWriteGuestReview_SB = TryFindResource("CloseWriteGuestReview_SB") as Storyboard;
            CloseWriteGuestReview_SB.Begin();
        }
    }
}
