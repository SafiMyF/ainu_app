﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AINU.UserControls
{
    /// <summary>
    /// Interaction logic for UserOptions.xaml
    /// </summary>
    public partial class UserOptions : UserControl
    {
        public UserOptions()
        {
            InitializeComponent();
        }
        public event EventHandler EvntOpenServices;
        public event EventHandler EvntOpenProducts;
        public event EventHandler EvntOpenRegForm;
        public event EventHandler EvntOpenInsurance;
        public event EventHandler EvntOpenFeedback;
        public event EventHandler EvntOpenDocList;
        public event EventHandler EvntOpenHealthTips;
        public event EventHandler EvntOpenFloorIndex;
        public event EventHandler EvntOpenReviews;
        public event EventHandler EvntOpenProcedures;


        private void btnServices_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntOpenServices(this, null);
        }

        private void btnProducts_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntOpenProducts(this, null);
        }

        private void btnRegForm_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntOpenRegForm(this, null);
        }

        private void btnInsurance_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntOpenInsurance(this, null);
        }

        private void btnDoctorsList_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntOpenDocList(this, null);
        }

        private void btnFeedback_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntOpenFeedback(this, null);
        }

        private void btnFloorInfo_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntOpenFloorIndex(this, null);
        }

        private void btnHealthTips_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntOpenHealthTips(this, null);
        }

        private void btnReviews_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntOpenReviews(this, null);
        }

        private void btnProceudres_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntOpenProcedures(this, null);
        }

        private void btnGallery_PreviewTouchUp(object sender, TouchEventArgs e)
        {

        }
    }
}
