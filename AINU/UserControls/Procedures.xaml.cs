﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace AINU.UserControls
{
    /// <summary>
    /// Interaction logic for Procedures.xaml
    /// </summary>
    public partial class Procedures : UserControl
    {
        public Procedures()
        {
            InitializeComponent();
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
        }
        private bool mediaPlayerIsPlaying = false;
        private bool userIsDraggingSlider = false;
        //public event EventHandler EvntOpenProcItemView;
        public event EventHandler EvntCloseProcedure;
        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntCloseProcedure(this, null);
        }
        private void btnItemListing_TouchDown_1(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;
            btnTouchedItem = btn;
            if (!isScrollMoved)
            {

            }
            else
                isScrollMoved = false;
        }


        Button btnTouchedItem = null;
        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        bool isScrollMoved = false;
        private void scrollViewerListing_ScrollChanged_1(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }
        }
        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void scrollViewerListing_PreviewTouchDown_1(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void scrollViewerListing_PreviewTouchMove_1(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void scrollViewerListing_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            if (touchScrollStartPoint != null && touchScrollEndPoint != null)
            {
                double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                if (diffValue <= 10)
                {
                    isScrollMoved = false;
                    //EvntOpenPrdItemView(btnTouchedItem, null);

                    //Storyboard to open Procedure Video Popup
                    Storyboard OpenProcVideoPopup_SB = TryFindResource("OpenProcVideoPopup_SB") as Storyboard;
                    OpenProcVideoPopup_SB.Begin();
                    MElmtSelectedProcVideo.Source = new Uri(@"ProcedureVideos/Nephrology - Kidney and Nephron.mp4", UriKind.Relative);
                    MElmtSelectedProcVideo.Play();
                }
            }
        }

        private void scrollViewerListing_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            MElmtProcedureItem.ScrubbingEnabled = true;
            MElmtProcedureItem.Pause();
            MElmtProcedureItem1.ScrubbingEnabled = true;
            MElmtProcedureItem1.Pause();

            MElmtProcedureItem2.ScrubbingEnabled = true;
            MElmtProcedureItem2.Pause();
            MElmtProcedureItem3.ScrubbingEnabled = true;
            MElmtProcedureItem3.Pause();
            MElmtProcedureItem4.ScrubbingEnabled = true;
            MElmtProcedureItem4.Pause();
            MElmtProcedureItem5.ScrubbingEnabled = true;
            MElmtProcedureItem5.Pause();
            MElmtProcedureItem6.ScrubbingEnabled = true;
            MElmtProcedureItem6.Pause();
            MElmtProcedureItem7.ScrubbingEnabled = true;
            MElmtProcedureItem7.Pause();
            MElmtProcedureItem8.ScrubbingEnabled = true;
            MElmtProcedureItem8.Pause();
            MElmtProcedureItem9.ScrubbingEnabled = true;
            MElmtProcedureItem9.Pause();
            MElmtProcedureItem10.ScrubbingEnabled = true;
            MElmtProcedureItem10.Pause();
        }

        #region ProcedureVideoControlEvents
        private void timer_Tick(object sender, EventArgs e)
        {
            if ((MElmtSelectedProcVideo.Source != null) && (MElmtSelectedProcVideo.NaturalDuration.HasTimeSpan) && (!userIsDraggingSlider))
            {
                sliProgress.Minimum = 0;
                sliProgress.Maximum = MElmtSelectedProcVideo.NaturalDuration.TimeSpan.TotalSeconds;
                sliProgress.Value = MElmtSelectedProcVideo.Position.TotalSeconds;
            }
        }
        private void MElmtSelectedProcVideo_MediaOpened(object sender, RoutedEventArgs e)
        {
            timelineSlider.Maximum = MElmtSelectedProcVideo.NaturalDuration.TimeSpan.TotalMilliseconds;
            Duration vdoDuration = MElmtSelectedProcVideo.NaturalDuration.TimeSpan.Duration();
            lblVideoLength.Text = vdoDuration.ToString();
        }
        private void MElmtSelectedProcVideo_MediaEnded(object sender, RoutedEventArgs e)
        {
            MElmtSelectedProcVideo.Position = new TimeSpan(0, 0, 1);
            MElmtSelectedProcVideo.Stop();
            mediaPlayerIsPlaying = false;
            btnPlay.Visibility = Visibility.Visible;
            btnPause.Visibility = Visibility.Collapsed;
            //Storyboard to close Procedure Video Popup
            Storyboard CloseProcVideoPopup_SB = TryFindResource("CloseProcVideoPopup_SB") as Storyboard;
            CloseProcVideoPopup_SB.Begin();
            mediaPlayerIsPlaying = false;
        }
        private void btnPlay_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            btnPlay.Visibility = Visibility.Collapsed;
            btnPause.Visibility = Visibility.Visible;
            MElmtSelectedProcVideo.Play();
            mediaPlayerIsPlaying = true;
        }

        private void btnPause_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            MElmtSelectedProcVideo.Pause();
            btnPause.Visibility = Visibility.Collapsed;
            btnPlay.Visibility = Visibility.Visible;
        }

        private void btnVolume_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            MElmtSelectedProcVideo.Volume = 0;
            volumeSlider.Value = 0;
            btnVolume.Visibility = Visibility.Collapsed;
            btnMute.Visibility = Visibility.Visible;

        }

        private void btnMute_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            MElmtSelectedProcVideo.Volume = 100;
            volumeSlider.Value = 100;
            btnMute.Visibility = Visibility.Collapsed;
            btnVolume.Visibility = Visibility.Visible;
        }

        private void volumeSlider_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            MElmtSelectedProcVideo.Volume = (double)volumeSlider.Value;
            if (MElmtSelectedProcVideo.Volume == 0)
            {
                btnVolume.Visibility = Visibility.Collapsed;
                btnMute.Visibility = Visibility.Visible;
            }
            if (MElmtSelectedProcVideo.Volume > 0)
            {
                btnMute.Visibility = Visibility.Collapsed;
                btnVolume.Visibility = Visibility.Visible;
            }

        }

        private void timelineSlider_ValueChanged_1(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int SliderValue = (int)timelineSlider.Value;

            // Overloaded constructor takes the arguments days, hours, minutes, seconds, miniseconds. 
            // Create a TimeSpan with miliseconds equal to the slider value.
            TimeSpan ts = new TimeSpan(0, 0, 0, 0, SliderValue);
            MElmtSelectedProcVideo.Position = ts;

        }
        private void sliProgress_DragStarted(object sender, DragStartedEventArgs e)
        {
            userIsDraggingSlider = true;
        }

        private void sliProgress_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            userIsDraggingSlider = false;
            MElmtSelectedProcVideo.Position = TimeSpan.FromSeconds(sliProgress.Value);
        }

        private void sliProgress_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            lblProgressStatus.Text = TimeSpan.FromSeconds(sliProgress.Value).ToString(@"hh\:mm\:ss");
        }
        #endregion

        private void btnCloseProcedurePopUp_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            MElmtSelectedProcVideo.Position = new TimeSpan(0, 0, 1);
            MElmtSelectedProcVideo.Stop();
            mediaPlayerIsPlaying = false;
            btnPlay.Visibility = Visibility.Collapsed;
            btnPause.Visibility = Visibility.Visible;
            //Storyboard to close Procedure Video Popup
            Storyboard CloseProcVideoPopup_SB = TryFindResource("CloseProcVideoPopup_SB") as Storyboard;
            CloseProcVideoPopup_SB.Begin();
            mediaPlayerIsPlaying = false;
        }
    }
}
