﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AINU.UserControls
{
    /// <summary>
    /// Interaction logic for Feedback.xaml
    /// </summary>
    public partial class Feedback : UserControl
    {
        public Feedback()
        {
            InitializeComponent();
        }
        public event EventHandler EvntCloseFeedback;
        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntCloseFeedback(this, null);
        }

        public Image imgTemp;
        public Image imgTemp1;
        private void btnSmiley1_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;  //gets the item selected
            grdSmileyResult1.Children.Clear();  //first clear the grdSmileyResult1
            imgTemp = new Image();
            imgTemp1 = btn.Content as Image;    // pass the button content as image to imgTemp1
            if (imgTemp1 != null)
            {
                imgTemp.Source = imgTemp1.Source;   
                grdSmileyResult1.Children.Add(imgTemp); //add the image to grid - grdSmileyResult1
            }
        }

        private void btnSmiley2_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;  //gets the item selected
            grdSmileyResult2.Children.Clear();  //first clear the grdSmileyResult2
            imgTemp = new Image();
            imgTemp1 = btn.Content as Image;
            if (imgTemp1 != null)
            {
                imgTemp.Source = imgTemp1.Source;
                grdSmileyResult2.Children.Add(imgTemp); //add the image to grid - grdSmileyResult2
            }

        }

        private void btnSmiley3_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;  //gets the item selected
            grdSmileyResult3.Children.Clear();  //first clear the grdSmileyResult3
            imgTemp = new Image();
            imgTemp1 = btn.Content as Image;    // pass the button content as image to imgTemp1
            if (imgTemp1 != null)
            {
                imgTemp.Source = imgTemp1.Source;
                grdSmileyResult3.Children.Add(imgTemp); //add the image to grid - grdSmileyResult3
            }
        }

        private void btnSmiley4_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;  //gets the item selected
            grdSmileyResult4.Children.Clear();  //first clear the grdSmileyResult4
            imgTemp = new Image();
            imgTemp1 = btn.Content as Image;    // pass the button content as image to imgTemp1
            if (imgTemp1 != null)
            {
                imgTemp.Source = imgTemp1.Source;
                grdSmileyResult4.Children.Add(imgTemp); //add the image to grid - grdSmileyResult4
            }
        }

        private void btnSmiley5_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;  //gets the item selected
            grdSmileyResult5.Children.Clear();  //first clear the grdSmileyResult5
            imgTemp = new Image();
            imgTemp1 = btn.Content as Image;    // pass the button content as image to imgTemp1
            if (imgTemp1 != null)
            {
                imgTemp.Source = imgTemp1.Source;
                grdSmileyResult5.Children.Add(imgTemp); //add the image to grid - grdSmileyResult5
            }
        }
        private void btnBackToImage_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            //close the feedback form storyboard
            Storyboard CloseFeedback_SB = TryFindResource("CloseFeedback_SB") as Storyboard;
            CloseFeedback_SB.Begin();
        }

        private void BannerImage_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            //open feedback from storyboard
            Storyboard OpenFeedback_SB = TryFindResource("OpenFeedback_SB") as Storyboard;
            OpenFeedback_SB.Begin();
        }
    }
}
