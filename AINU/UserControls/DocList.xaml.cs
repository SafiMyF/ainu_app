﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace AINU.UserControls
{    
    /// <summary>
    /// Interaction logic for DocList.xaml
    /// </summary>
    public partial class DocList : UserControl
    {
        public DocList()
        {
            InitializeComponent();
        }
        public event EventHandler EvntCloseDocList;
        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntCloseDocList(this, null);
        }

        private void svDocList_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void btnNephrology_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            //Opens DocList of Nephrology Department and hides otherlist
            grdGeneral.Visibility = Visibility.Collapsed;
            grdAnesthesiologyCriticalCare.Visibility = Visibility.Collapsed;
            grdNephrology.Visibility = Visibility.Visible;
            grdRadiology.Visibility = Visibility.Collapsed;
            grdUrology.Visibility = Visibility.Collapsed;
        }

        private void btnUrology_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            //Opens DocList of Urology Department and hides otherlist
            grdGeneral.Visibility = Visibility.Collapsed;
            grdAnesthesiologyCriticalCare.Visibility = Visibility.Collapsed;
            grdNephrology.Visibility = Visibility.Collapsed;
            grdRadiology.Visibility = Visibility.Collapsed;
            grdUrology.Visibility = Visibility.Visible;
        }

        private void btnAnesthesiologyCriticalCare_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            //Opens DocList of AnesthesiologyCriticalCare Department and hides otherlist
            grdGeneral.Visibility = Visibility.Collapsed;
            grdAnesthesiologyCriticalCare.Visibility = Visibility.Visible;
            grdNephrology.Visibility = Visibility.Collapsed;
            grdRadiology.Visibility = Visibility.Collapsed;
            grdUrology.Visibility = Visibility.Collapsed;
        }

        private void btnRadiology_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            //Opens DocList of Radiology Department and hides otherlist
            grdGeneral.Visibility = Visibility.Collapsed;
            grdAnesthesiologyCriticalCare.Visibility = Visibility.Collapsed;
            grdNephrology.Visibility = Visibility.Collapsed;
            grdRadiology.Visibility = Visibility.Visible;
            grdUrology.Visibility = Visibility.Collapsed;
        }

        private void btnGeneral_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            //Opens DocList of General Department and hides otherlist
            grdGeneral.Visibility = Visibility.Visible;
            grdAnesthesiologyCriticalCare.Visibility = Visibility.Collapsed;
            grdNephrology.Visibility = Visibility.Collapsed;
            grdRadiology.Visibility = Visibility.Collapsed;
            grdUrology.Visibility = Visibility.Collapsed;
        }

        private void btnDocDetails_TouchDown(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;
            btnTouchedItem = btn;
            if (!isScrollMoved)
            {

            }
            else
                isScrollMoved = false;
        }
        Button btnTouchedItem = null;
        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        bool isScrollMoved = false;

        private void svDocList2_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }
        }
        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void svDocList2_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svDocList2_PreviewTouchMove(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void svDocList2_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            if (touchScrollStartPoint != null && touchScrollEndPoint != null)
            {
                double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                if (diffValue <= 10)
                {
                    isScrollMoved = false;
                    Storyboard OpenDocDetailsPopUp_SB = TryFindResource("OpenDocDetailsPopUp_SB") as Storyboard;
                    OpenDocDetailsPopUp_SB.Begin();
                }
            }
        }

        private void btnCloseDocDetailsPopUp_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            Storyboard CloseDocDetailsPopUp_SB = TryFindResource("CloseDocDetailsPopUp_SB") as Storyboard;
            CloseDocDetailsPopUp_SB.Begin();
        }
    }
}
