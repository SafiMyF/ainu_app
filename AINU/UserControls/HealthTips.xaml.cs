﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AINU.UserControls
{
    /// <summary>
    /// Interaction logic for HealthTips.xaml
    /// </summary>
    public partial class HealthTips : UserControl
    {
        public HealthTips()
        {
            InitializeComponent();
        }
        public event EventHandler EvntOpenHealthTipItem;
        public event EventHandler EvntCloseHealthTips;
        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntCloseHealthTips(this, null);
        }

        private void btnItemListing_TouchDown_1(object sender, TouchEventArgs e)
        {
            Button btn = sender as Button;
            btnTouchedItem = btn;
            if (!isScrollMoved)
            {

            }
            else
                isScrollMoved = false;
        }
        Button btnTouchedItem = null;
        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        bool isScrollMoved = false;
        private void scrollViewerListing_ScrollChanged_1(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }
        }
        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void scrollViewerListing_PreviewTouchDown_1(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void scrollViewerListing_PreviewTouchMove_1(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void scrollViewerListing_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            if (touchScrollStartPoint != null && touchScrollEndPoint != null)
            {
                double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                if (diffValue <= 10)
                {
                    isScrollMoved = false;
                    EvntOpenHealthTipItem(btnTouchedItem, null);
                }
            }
        }

        private void scrollViewerListing_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
