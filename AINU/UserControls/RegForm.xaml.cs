﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace AINU.UserControls
{
    /// <summary>
    /// Interaction logic for RegForm.xaml
    /// </summary>
    public partial class RegForm : UserControl
    {
        public RegForm()
        {
            InitializeComponent();
        }
        public event EventHandler EvntCloseRegForm;

        private void btnBack_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            EvntCloseRegForm(this, null);  
        }
    }
}
