﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AINU
{
    /// <summary>
    /// Interaction logic for SplashScreen.xaml
    /// </summary>
    public partial class SplashScreen : Window
    {
        public SplashScreen()
        {
            InitializeComponent();
        }
        BackgroundWorker bgw = null;
        private void Window_ContentRendered(object sender, EventArgs e)
        {
            bgw = new BackgroundWorker();
            bgw.WorkerReportsProgress = true;
            bgw.DoWork += bgw_DoWork;
            bgw.ProgressChanged += bgw_ProgressChanged;
            bgw.RunWorkerAsync();
        }
        void bgw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pbSplashScreen.Value = e.ProgressPercentage;
            if (pbSplashScreen.Value == 20)
            {
                this.Hide();
                MainWindow HomePage = new MainWindow();
                HomePage.ShowDialog();
                this.Close();
            }
        }
        void bgw_DoWork(object sender, DoWorkEventArgs e)
        {
            for (int i = 0; i <= 20; i++)
            {
                (sender as BackgroundWorker).ReportProgress(i);
                Thread.Sleep(50);
            }
        }
    }
}
