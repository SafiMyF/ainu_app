﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AINU.UserControls;

namespace AINU
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void grdHomePanel_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            LoadUserOptions();  //method to load UserOptions page
        }

        private void LoadUserOptions()
        {
            Storyboard HomeAnimi = TryFindResource("HomeAnimi") as Storyboard;  //Homepage slideshow storyboard
            HomeAnimi.Stop();
            LayoutRoot.Visibility = Visibility.Collapsed;
            grdHomePanel.Visibility = Visibility.Collapsed;
            grdParentPanel.Children.Clear();    //clear all children from grdParentPanel and load UserOptions Page

            UserOptions usrOpt = new UserOptions();
            grdParentPanel.Children.Add(usrOpt);
            //Events raised from UserOptions.xaml
            usrOpt.EvntOpenServices += usrOpt_EvntOpenServices;
            usrOpt.EvntOpenRegForm += usrOpt_EvntOpenRegForm;
            usrOpt.EvntOpenInsurance += usrOpt_EvntOpenInsurance;
            usrOpt.EvntOpenFeedback += usrOpt_EvntOpenFeedback;
            usrOpt.EvntOpenDocList += usrOpt_EvntOpenDocList;
            usrOpt.EvntOpenHealthTips += usrOpt_EvntOpenHealthTips;
            usrOpt.EvntOpenFloorIndex += usrOpt_EvntOpenFloorIndex;
            usrOpt.EvntOpenReviews += usrOpt_EvntOpenReviews;
            usrOpt.EvntOpenProcedures += usrOpt_EvntOpenProcedures;
        }

        void usrOpt_EvntOpenServices(object sender, EventArgs e)
        {
            Services serv = new Services();
            grdParentPanel.Children.Add(serv);  // add Services.xaml to grdParentPanel
            //Event raised from services page
            serv.EvntCloseServices += serv_EvntCloseServices;
        }

        void serv_EvntCloseServices(object sender, EventArgs e)
        {
            //remove Services.xaml from parent panel
            Services serv = (Services)sender;
            grdParentPanel.Children.Remove(serv);
        }
        void usrOpt_EvntOpenRegForm(object sender, EventArgs e)
        {
            RegForm regF = new RegForm();
            grdParentPanel.Children.Add(regF);  //Add RegForm.xaml to grdParentPanel
            //Event raised from RegForm page
            regF.EvntCloseRegForm += regF_EvntCloseRegForm;
        }

        void regF_EvntCloseRegForm(object sender, EventArgs e)
        {
            //remove RegForm.xaml from parent panel
            RegForm regF = (RegForm)sender;
            grdParentPanel.Children.Remove(regF);
        }
        void usrOpt_EvntOpenInsurance(object sender, EventArgs e)
        {
            Insurance inc = new Insurance();
            grdParentPanel.Children.Add(inc);  //Add Insurance.xaml to grdParentPanel
            //Events raised from Insurance page
            inc.EvntCloseInsurance += inc_EvntCloseInsurance;
        }

        void inc_EvntCloseInsurance(object sender, EventArgs e)
        {
            //remove insurance.xaml from parent panel
            Insurance inc = (Insurance)sender;
            grdParentPanel.Children.Remove(inc);
        }
        void usrOpt_EvntOpenFeedback(object sender, EventArgs e)
        {
            Feedback feedBk = new Feedback();
            grdParentPanel.Children.Add(feedBk);  //Add Feedback.xaml to grdParentPanel
            //Events raised from Feedback
            feedBk.EvntCloseFeedback += feedBk_EvntCloseFeedback;
        }

        void feedBk_EvntCloseFeedback(object sender, EventArgs e)
        {
            //remove feedback.xaml from parent panel
            Feedback feedBk = (Feedback)sender;
            grdParentPanel.Children.Remove(feedBk);
        }
        void usrOpt_EvntOpenDocList(object sender, EventArgs e)
        {
            DocList dcLst = new DocList();
            grdParentPanel.Children.Add(dcLst);  //Add DocList.xaml to grdParentPanel
            //Events raised from DocList
            dcLst.EvntCloseDocList += dcLst_EvntCloseDocList;
        }

        void dcLst_EvntCloseDocList(object sender, EventArgs e)
        {
            //remove doclist.xaml form parent panel
            DocList dcLst = (DocList)sender;
            grdParentPanel.Children.Remove(dcLst);
        }
        void usrOpt_EvntOpenHealthTips(object sender, EventArgs e)
        {
            HealthTips htips = new HealthTips();
            grdParentPanel.Children.Add(htips); //add HealthTips.xaml to grdParentPanel
            //Events raised from HealthTips.xaml
            htips.EvntOpenHealthTipItem += htips_EvntOpenHealthTipItem;
            htips.EvntCloseHealthTips += htips_EvntCloseHealthTips;
        }

        void htips_EvntCloseHealthTips(object sender, EventArgs e)
        {
            //remove HealthTips.xaml page from parent panel
            HealthTips htips = (HealthTips)sender;
            grdParentPanel.Children.Remove(htips);
        }
        void htips_EvntOpenHealthTipItem(object sender, EventArgs e)
        {
            Button btnDets = sender as Button;
            if (btnDets != null)
            {
                HealthTipItemView hTipIV = new HealthTipItemView();
                hTipIV.PlanViewImg = btnDets;   //assign btnDets(contains selected item) value to hTipIV.PlanViewImg
                //HealthTipItemView.xaml below properties used to zoom the object
                hTipIV.IsManipulationEnabled = true;
                hTipIV.RenderTransform = new MatrixTransform(1, 0, 0, 1, 20, 20);
                hTipIV.BringIntoView();
                Panel.SetZIndex(hTipIV, 9999);
                grdParentPanel.Children.Add(hTipIV);    //add hTipIV to grdParentPanel
                //Events raised from HealthTipItemView.xaml
                hTipIV.EvntCloseHealthTipIV += hTipIV_EvntCloseHealthTipIV;
            }
        }
        void hTipIV_EvntCloseHealthTipIV(object sender, EventArgs e)
        {
            HealthTipItemView hTipIV = (HealthTipItemView)sender;
            grdParentPanel.Children.Remove(hTipIV); //close selected hTipIV
        }
        void usrOpt_EvntOpenFloorIndex(object sender, EventArgs e)
        {
            FloorIndex fIndex = new FloorIndex();
            grdParentPanel.Children.Add(fIndex);    //add FloorIndex.xaml to grdParentPanel
            //Events raised from FloorIndex page
            fIndex.EvntCloseFloorIndex += fIndex_EvntCloseFloorIndex;
        }

        void fIndex_EvntCloseFloorIndex(object sender, EventArgs e)
        {
            //remove floorindex.xaml from parent panel.
            FloorIndex fIndex = (FloorIndex)sender;
            grdParentPanel.Children.Remove(fIndex);
        }
        void usrOpt_EvntOpenReviews(object sender, EventArgs e)
        {
            Reviews review = new Reviews();
            grdParentPanel.Children.Add(review);
            //Events raised from Reviews
            review.EvntCloseReviews += review_EvntCloseReviews;
        }

        void review_EvntCloseReviews(object sender, EventArgs e)
        {
            Reviews review = (Reviews)sender;
            grdParentPanel.Children.Remove(review);
        }
        void usrOpt_EvntOpenProcedures(object sender, EventArgs e)
        {
            Procedures proc = new Procedures();
            grdParentPanel.Children.Add(proc);
            //Events raised from Procedures.
            proc.EvntCloseProcedure += proc_EvntCloseProcedure;
        }
        void proc_EvntCloseProcedure(object sender, EventArgs e)
        {
            Procedures proc = (Procedures)sender;
            grdParentPanel.Children.Remove(proc);
        }

        private void btnHome_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            grdParentPanel.Children.Clear();    //clear all children from grdParentPanel
            Storyboard HomeAnimi = TryFindResource("HomeAnimi") as Storyboard;  //homepage slideshow find and starts.
            HomeAnimi.Begin();
            LayoutRoot.Visibility = Visibility.Visible;
            grdHomePanel.Visibility = Visibility.Visible;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            grdParentPanel.Children.Clear();    //clear all children from grdParentPanel
            Storyboard HomeAnimi = TryFindResource("HomeAnimi") as Storyboard;  //homepage slideshow find and starts.
            HomeAnimi.Begin();
            LayoutRoot.Visibility = Visibility.Visible;

            //Raise Events to manipulate objects.
            this.ManipulationStarting -= MainWindow_ManipulationStarting;
            this.ManipulationDelta -= MainWindow_ManipulationDelta;
            this.ManipulationStarting += MainWindow_ManipulationStarting;
            this.ManipulationDelta += MainWindow_ManipulationDelta;
        }

        ManipulationModes currentMode = ManipulationModes.All;
        void MainWindow_ManipulationDelta(object sender, ManipulationDeltaEventArgs args)
        {
            var element = args.OriginalSource as UIElement;

            var transformation = element.RenderTransform
                                                 as MatrixTransform;
            var matrix = transformation == null ? Matrix.Identity :
                                           transformation.Matrix;

            //set ScaleAt
            matrix.ScaleAt(args.DeltaManipulation.Scale.X,
                           args.DeltaManipulation.Scale.Y,
                           args.ManipulationOrigin.X,
                           args.ManipulationOrigin.Y);

            //matrix.RotateAt(args.DeltaManipulation.Rotation,
            //                args.ManipulationOrigin.X,
            //                args.ManipulationOrigin.Y);

            //set Translate
            matrix.Translate(args.DeltaManipulation.Translation.X,
                             args.DeltaManipulation.Translation.Y);

            element.RenderTransform = new MatrixTransform(matrix);
            args.Handled = true;
        }

        void MainWindow_ManipulationStarting(object sender, ManipulationStartingEventArgs args)
        {
            args.ManipulationContainer = this;
            args.Mode = currentMode;

            // Adjust Z-order
            FrameworkElement element = args.Source as FrameworkElement;
            Panel pnl = element.Parent as Panel;

            for (int i = 0; i < pnl.Children.Count; i++)
                Panel.SetZIndex(pnl.Children[i],
                    pnl.Children[i] == element ? pnl.Children.Count : i);

            // Set Pivot
            Point center = new Point(element.ActualWidth / 2, element.ActualHeight / 2);
            center = element.TranslatePoint(center, this);

            args.Pivot = new ManipulationPivot(center, 48);

            args.Handled = true;
            base.OnManipulationStarting(args);
        }
    }
}
